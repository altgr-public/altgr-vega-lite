from datetime import datetime


VegaDatetime = dict[str, float]
Interval = dict[str, datetime]


def vega_datetime_from_datetime(datetime: datetime) -> VegaDatetime:
    return {
        'year': datetime.year,
        'month': datetime.month,
        'date': datetime.day,
        'hours': datetime.hour,
        'minutes': datetime.minute,
        'seconds': datetime.second,
        'milliseconds': datetime.microsecond // 1000
    }


def domain_from_interval(interval: Interval):
    start = vega_datetime_from_datetime(interval['start'])
    stop = vega_datetime_from_datetime(interval['stop'])
    return [start, stop]


