"""
Vega Lite functions.
"""
from datetime import datetime
import json
import re
from typing import Any


Interval = dict[str, datetime]
VegaLiteSpec = dict[str, Any]


def load_spec(
        filepath: str,
        data: dict[str, Any] = {},
        palette: dict[str, str] = {}
) -> VegaLiteSpec:
    print(f"[load_spec] data : {data}")
    data = data | palette
    return load_vega_lite_spec(filepath, data=data)


def load_vega_lite_spec(
        filepath: str,
        data: dict[str, Any] = {}
) -> VegaLiteSpec:
    """Loads Vega Lite specifications and substitues values in template.

    Variable substitution: "foo": "$bar$"
    Comments: "// This is a comment \n".
    """
    with open(filepath, 'r') as json_file:
        lines = remove_comments(json_file.read())
        spec = json.loads(lines)
        substitute_in_dictionary(spec, data)
        return spec


def remove_comments(x: str):
    """Remove all comments from a string."""
    return re.sub('//[^\n]*\n', '\n', x)


def substitute_in_dictionary(
        d: dict[str, Any],
        data: dict[str, Any] = {}
) -> None:
    for key, x in sorted(d.items(), key=lambda x: x[0]):
        if isinstance(x, dict):
            substitute_in_dictionary(x, data)
        elif isinstance(x, list):
            substitute_in_list(x, data)
        elif is_variable(x):
            trimmed = trim_dollars(x)
            d[key] = data[trimmed]


def substitute_in_list(
        xs: list[str, Any],
        data: dict[str, Any] = {}
) -> None:
    for i, x in enumerate(xs):
        if isinstance(x, dict):
            substitute_in_dictionary(x, data)
        elif isinstance(x, list):
            substitute_in_list(x, data)
        elif is_variable(x):
            trimmed = trim_dollars(x)
            xs[i] = data[trimmed]


def trim_dollars(x: str) -> str:
    return x[1:-1]


def is_variable(x: str) -> bool:
    """Could have used regexp instead."""
    return isinstance(x, str) and len(x) > 2 and x[0] == '$' and x[-1] == '$'


def domain_from_interval(interval: Interval):
    return [
        vega_lite_object_from_datetime(interval['begin']),
        vega_lite_object_from_datetime(interval['end'])
    ]


def vega_lite_object_from_datetime(dt: datetime):
    return {
        'year': dt.year,
        'month': dt.month,
        'date': dt.day,
        'hours': dt.hour,
        'minutes': dt.minute,
        'seconds': dt.second
    }
