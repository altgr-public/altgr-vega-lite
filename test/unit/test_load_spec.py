"""
Unit tests for Vega Lite spec loader.
"""
import copy
import pytest


from altgr_vega_lite.load_spec import (
    is_variable,
    remove_comments,
    substitute_in_dictionary,
    trim_dollars
)


def test_remove_comments():
    x = 'foo: bar // walou\n baz: corge'
    assert remove_comments(x) == 'foo: bar \n baz: corge'


def test_is_variable():
    assert is_variable('$foo$')
    assert not is_variable('foo')
    assert not is_variable('$foo')
    assert not is_variable('foo$')


def test_trim_dollars():
    assert trim_dollars('$foo$') == 'foo'


def test_substitute_in_dictionary_do_nothing_str():
    spec = {'foo': 'bar'}
    spec_copied = copy.deepcopy(spec)
    substitute_in_dictionary(spec)
    assert spec == spec_copied


def test_substitute_in_dictionary_do_nothing_int():
    spec = {'foo': 43}
    spec_copied = copy.deepcopy(spec)
    substitute_in_dictionary(spec, {'x': 42})
    assert spec == spec_copied


def test_substitute_in_dictionary_dict():
    spec = {'foo': '$x$'}
    substitute_in_dictionary(spec, {'x': 42})
    assert spec == {'foo': 42}


def test_substitute_in_dictionary_dict_missing_key():
    spec = {'foo': '$y$'}
    with pytest.raises(KeyError):
        assert substitute_in_dictionary(spec, {'x': 42})


def test_substitute_in_dictionary_dict_of_dict():
    spec = {'foo': {'bar': '$x$'}}
    substitute_in_dictionary(spec, {'x': 42})
    assert spec == {'foo': {'bar': 42}}


def test_substitute_in_dictionary_dict_of_dict_of_list():
    spec = {'foo': {'bar': ['baz', '$x$']}}
    substitute_in_dictionary(spec, {'x': 42})
    assert spec == {'foo': {'bar': ['baz', 42]}}


def test_substitute_in_dictionary_dict_of_list_of_dict_of_list():
    spec = {'foo': [{'bar': ['baz', '$x$']}]}
    substitute_in_dictionary(spec, {'x': 42})
    assert spec == {'foo': [{'bar': ['baz', 42]}]}
